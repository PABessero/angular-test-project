import { Component, OnInit } from '@angular/core';
import { CARDS } from "../mock-cards";
import {Card} from "../card";

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {

  constructor() { }
  cards: Card[] = CARDS;

  ngOnInit(): void {
  }

}
