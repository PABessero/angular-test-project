export interface Card {
  title: string;
  content?: string;
  width: number;
  height: number;
}
