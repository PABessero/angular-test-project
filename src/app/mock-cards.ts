import { Card } from "./card";

export const CARDS: Card[] = [
  { title: 'Card 1', width: 1, height: 1, content: 'A 1 by 1 card' },
  { title: 'Card 2', width: 1, height: 1, content: 'A 1 by 1 card' },
  { title: 'Card 3', width: 1, height: 1, content: 'A 1 by 1 card' },
  { title: 'Card 4', width: 1, height: 2, content: 'A 1 by 2 card' },
  { title: 'Card 5', width: 2, height: 2, content: 'A 2 by 2 card' },
  { title: 'Card 6', width: 4, height: 2, content: 'A full width card' },

]
